package com.hy.config;

import com.hy.bean.RefreshIdGeneratorListener;
import com.hy.corecode.idgen.WFGIdGenerator;
import com.hy.properties.IdGeneratorOptions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;



/**
 * @author: 王富贵
 * @description: id生成自动配置类
 * @createTime: 2022年05月19日 11:11:27
 */
@AutoConfiguration
@EnableConfigurationProperties({IdGeneratorOptions.class})
public class WFGIdGeneratorConfig {

    /**
     * 自动装配的配置
     *
     * @return DefaultIdGenerator
     */
    @Bean
    @ConditionalOnMissingBean(WFGIdGenerator.class)// 用户没有注入自己的bean才装配
    public WFGIdGenerator getDefaultIdGenerator(
            @Qualifier("idGeneratorOptions") IdGeneratorOptions idGeneratorOptions) {
        return new WFGIdGenerator(idGeneratorOptions);
    }

    /**
     * 刷新静态id生成器监听器
     *
     * @return RefreshIdGeneratorListener
     */
    @Bean
    @ConditionalOnMissingBean(RefreshIdGeneratorListener.class)
    public RefreshIdGeneratorListener refreshIdGeneratorListener() {
        return new RefreshIdGeneratorListener();
    }
}