package com.hy.corecode.idgen;

/**
 * @author: 王富贵
 * @description: 静态的id生成器
 * @createTime: 2023/9/4 18:42
 */
public class IdGenerator {

    // 内部代理的生成器
    private static WFGIdGenerator wfgIdGenerator = null;

    /**
     * 生产一个id
     * @return 生产id
     */
    public static long next() {
        return wfgIdGenerator.next();
    }

    public static void setWfgIdGenerator(WFGIdGenerator wfgIdGenerator) {
        IdGenerator.wfgIdGenerator = wfgIdGenerator;
    }
}
